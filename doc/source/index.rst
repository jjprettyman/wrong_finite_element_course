
This is the webpage for the `Imperial College London Mathematics
<http://www.imperial.ac.uk/maths>`_ masters module M5MA47 Finite
Elements: numerical analysis and implementation. Other people are
welcome to make use of the material here. The authors welcome feedback
and would particularly appreciate an `email
<mailto:david.ham@imperial.ac.uk>`_ if this material is used to teach
anywhere.

.. toctree::

   practicalities

Part 1: Numerical analysis
--------------------------

The theory part of the module will consist of two hours per week
primarily composed of letures, with occasional tutorials. This part of
the module will be led by `Dr Colin Cotter
<http://www.imperial.ac.uk/people/colin.cotter>`_.

The text for this part of the module is Brenner and Scott *The
Mathematical Theory of Finite Element Methods*. Imperial College has
fortunately paid for PDF access to this book, so it is accessible from
the Imperial College network at `Springer Link
<http://link.springer.com/book/10.1007%2F978-0-387-75934-0>`_.

Part 2: Implementation
----------------------

The implementation part of the module aims to give the students a
deeper understanding of the finite element method through writing
software to solve finite element problems in one and two dimensions.

This part of the module will be taught by `Dr David Ham
<http://www.imperial.ac.uk/people/david.ham>`_ in two hours per week
of computer laboratory time.

.. toctree::
   :maxdepth: 3

   tools
   implementation

Implementation exercise contents:

.. toctree::
   :numbered:
   :maxdepth: 2

   quadrature
   finite_elements
   meshes
   function_spaces
   functions
   zbibliography
